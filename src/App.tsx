import { Routes, Route, Link } from "react-router-dom";

import { MainPage } from "./pages/MainPage/";
import { WeatherPage } from "./pages/WeatherPage";
import { TodoPage } from "./pages/TodoPage";

import "./App.css";

function App() {
  return (
    <div>
      <div id="navContainer">
        <h2>
          <Link to="/converter">Converter</Link>
        </h2>
        <h2>
          <Link to="/todo">ToDo list</Link>
        </h2>
        <h2>
          <Link to="/weather">Weather</Link>
        </h2>
      </div>

      <Routes>
        <Route path="/converter" element={<MainPage />} />
        <Route path="/weather" element={<WeatherPage />} />
        <Route path="/todo" element={<TodoPage />} />
      </Routes>
    </div>
  );
}

export default App;
