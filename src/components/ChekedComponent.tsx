import React from "react";

import "./CheckedComponent.css";

export function ChekedComponent(props: any) {
  const [checked, setChecked] = React.useState<boolean>(false);
  return (
    <div id="todoItem">
      <ul>
        <li id={checked ? "todoText" : undefined}>{props.todo}</li>
      </ul>
      <input
        type="checkbox"
        checked={checked}
        onChange={() => setChecked(!checked)}
      />
    </div>
  );
}
