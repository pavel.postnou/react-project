import React from "react";
import "./MainPage.css";

export function MainPage() {
  const [item, setItem] = React.useState<string[] | null | undefined>([]);
  const [currencies, setCurrencies] = React.useState<
    { code: string; value: number }[]
  >([]);
  const [firstCurrency, setFirstCurrency] = React.useState<string>();
  const [secondCurrency, setSecondCurrency] = React.useState<string>();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [firstValue, setFirstValue] = React.useState<
    string | number | readonly string[] | undefined
  >(0);
  const [secondValue, setSecondValue] = React.useState<
    string | number | readonly string[] | undefined
  >(0);
  const valOne = React.useRef<HTMLInputElement>(null);
  const valTwo = React.useRef<HTMLInputElement>(null);

  const selectOne: HTMLSelectElement = document.getElementById(
    "select1"
  ) as HTMLSelectElement;
  const selectTwo: HTMLSelectElement = document.getElementById(
    "select2"
  ) as HTMLSelectElement;
  const saveCurrencies = (data: [{ code: string; value: number }]) => {
    const arr = [];
    for (let key in data) {
      arr.push(data[key]);
    }
    setCurrencies(arr);
  };

  React.useEffect(() => {
    const data = JSON.parse(localStorage.getItem("item")!);
    setItem(data);
  }, []);

  React.useEffect(() => {
    if (currencies.length !== 0) {
      if (selectTwo.selectedIndex !== -1 && selectOne.selectedIndex !== -1) {
        const arr = item?.slice();
        if (arr !== undefined && arr.length < 5) {
          arr.push(
            `чтобы купить ${secondValue} ${
              currencies[selectTwo.selectedIndex].code
            } нужно ${firstValue} ${currencies[selectOne.selectedIndex].code}`
          );
          localStorage.setItem("item", JSON.stringify(arr));
          setItem(arr);
        } else if (arr !== undefined && arr.length === 5) {
          arr.shift();
          arr.push(
            `чтобы купить ${secondValue} ${
              currencies[selectTwo.selectedIndex].code
            } нужно ${firstValue} ${currencies[selectOne.selectedIndex].code}`
          );
          localStorage.setItem("item", JSON.stringify(arr));
          setItem(arr);
        }
      }
    }
  }, [firstValue, secondValue]);

  const getCurrencies = () => {
    setLoading(true);
    fetch(
      "https://api.currencyapi.com/v3/latest?apikey=d20cbae0-5cbc-11ec-be53-f5c295280e78"
    )
      .then((resp) => resp.json())
      .then((data) => (saveCurrencies(data.data), setLoading(false)))
      .catch((error) => alert(error));
  };

  const convert = (id: number, e: string | undefined) => {
    if (id === 1) {
      setFirstValue(e);
      const sum: number = Math.round(
        (Number(e) * Number(selectOne.value)) / Number(selectTwo.value)
      );
      setSecondValue(sum);
    } else if (id === 2) {
      setSecondValue(e);
      const sum: number = Math.round(
        (Number(e) * Number(selectTwo.value)) / Number(selectOne.value)
      );
      setFirstValue(sum);
    }
  };

  return (
    <div className="currencyContainer">
      <div className="selectdiv">
        <button id="button" onClick={getCurrencies}>
          GET CURRENCIES
        </button>
        {loading ? (
          <p>Loading...</p>
        ) : (
          <div id="list">
            <ul>
              {currencies.map((cur, index) => (
                <li key={index} id="listcurrency">
                  <p id="currency">{cur.code}</p>
                  <p id="value">{cur.value}</p>
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
      <div className="selectdiv">
        <select
          id="select1"
          className="input"
          name="list"
          onChange={() => (
            setFirstCurrency(selectOne.value), convert(1, valOne.current?.value)
          )}
        >
          {currencies.map((cur, index) => (
            <option key={index} value={cur.value}>
              {cur.code}
            </option>
          ))}
        </select>
        <p>у меня есть</p>
        <input
          className="input"
          type="number"
          min="0"
          ref={valOne}
          value={firstValue}
          onChange={(e) => convert(1, e.currentTarget.value)}
          placeholder="input amount"
        />
        <p>{firstCurrency}</p>
        <ul>
          {item?.map((text, index) => (
            <li key={index}>{text}</li>
          ))}
        </ul>
      </div>
      <div className="selectdiv">
        <select
          id="select2"
          className="input"
          name="list"
          onChange={() => (
            setSecondCurrency(selectTwo.value),
            convert(2, valTwo.current?.value)
          )}
        >
          {currencies.map((cur, index) => (
            <option key={index} value={cur.value}>
              {cur.code}
            </option>
          ))}
        </select>
        <p>хочу купить</p>
        <input
          className="input"
          type="number"
          min="0"
          ref={valTwo}
          value={secondValue}
          onChange={(e) => convert(2, e.currentTarget.value)}
          placeholder="input amount"
        />
        <p>{secondCurrency}</p>
      </div>
    </div>
  );
}
