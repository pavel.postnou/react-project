import React from "react";
import "./TodoPage.css";

import { FaPlus } from "react-icons/fa";

import { ChekedComponent } from "../../components/ChekedComponent";

export function TodoPage() {
  const [isHide, setIsHide] = React.useState<boolean>(true);
  const [todoList, setTodoList] = React.useState<string[]>([]);
  const todoRef = React.useRef<HTMLInputElement>(null);

  const addTodo = () => {
    const arr: string[] = todoList.slice();
    if (todoRef.current !== null && todoRef.current.value !== "") {
      arr.push(todoRef.current.value);
      setTodoList(arr);
      todoRef.current.value = "";
    }
  };

  return (
      <div id="todoCnt">
        <div hidden={isHide}>
          <input ref={todoRef} placeholder="write your todo"></input>
          <button onClick={addTodo}>ADD TODO</button>
          <button onClick={() => setIsHide(true)}>SAVE</button>
        </div>
        <div>
          {todoList
            ? todoList.map((todo, index) => (
                <ChekedComponent key={index} todo={todo} onChange={onchange} />
              ))
            : null}
        </div>
        <br></br>
        <div id="iconCnt">
          <FaPlus size={40} color="blue" onClick={() => setIsHide(false)} />
        </div>
      </div>
  );
}
