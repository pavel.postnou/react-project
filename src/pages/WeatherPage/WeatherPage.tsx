import React from "react";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import "./WeatherPage.css";
interface main {
  temp: string;
  humidity: number;
  pressure: number;
}

interface weather {
  0: { description: string; icon: string; main: string };
}

interface wind {
  speed: number;
}
interface data {
  main: main;
  name: string;
  weather: weather;
  wind: wind;
}
export function WeatherPage() {
  const [data, setData] = React.useState<data | null>(null);
  const [startDate, setStartDate] = React.useState(new Date());

  React.useEffect(() => {
    fetch(
      "https://api.openweathermap.org/data/2.5/weather?q=Minsk&limit-5&lang=ru&appid=6b65b2eeab376aff39c08df8023f48fc&units=metric"
    )
      .then((resp: Response) => resp.json())
      .then((data: data) => setData(data));
  }, []);

  const todayWeather = () => {
    fetch(
      "https://api.openweathermap.org/data/2.5/weather?q=Minsk&limit-5&lang=ru&appid=6b65b2eeab376aff39c08df8023f48fc&units=metric"
    )
      .then((resp: Response) => resp.json())
      .then((data: data) => setData(data));
  };

  const historicalWeather = (date: Date) => {
    const dt = String(date);
    const sec = Date.parse(dt) / 1000;
    fetch(
      `https://history.openweathermap.org/data/3.0/history/timemachine?lat=51.51&lon=-0.13&dt=606348800&appid=6b65b2eeab376aff39c08df8023f48fc`
    )
      .then((resp: Response) => resp.json())
      .then((data: data) => console.log(data));
  };

  return (
    <div id="mainDiv">
      {data ? (
        <div id="weatherDiv">
          <DatePicker
            selected={startDate}
            onChange={(date: Date) => (
              setStartDate(date), historicalWeather(date)
            )}
          />
          <input
            id="submitBtn"
            type="button"
            value="Погода сегодня"
            onClick={todayWeather}
          />
          <h2>Минск</h2>
          <ul>
            <li className="weatherParams">температура {data?.main.temp} °C</li>
            <li className="weatherParams">влажность {data?.main.humidity} %</li>
            <li className="weatherParams">
              давление {data?.main.pressure} мм рт. ст.
            </li>
            <li className="weatherParams">{data?.weather[0].description}</li>
            <li className="weatherParams">ветер {data?.wind.speed} м/с</li>
          </ul>
        </div>
      ) : null}
    </div>
  );
}
